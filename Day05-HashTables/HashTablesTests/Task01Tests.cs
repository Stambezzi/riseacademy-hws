namespace HashTablesTests;
using Day05_HashTables;

[TestClass]
public class Task01Tests
{
    [TestMethod]
    public void TestCoolestPlate()
    {
        //Assemble
        Dictionary<string, string> plates = new Dictionary<string, string>();
        plates.Add("CB0281PK", "John Smith");
        plates.Add("OB2981MK", "Suzana Smith");
        plates.Add("PK9174MM", "Rachel Brown");
        plates.Add("OB0198PO", "Racer Racerson");

        string expected = "Rachel Brown: PK9174MM";

        //Act
        string result = TaskSolutions.GetCoolestOwner(plates);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestCoolestPlateWithMultipleCarsToOwner()
    {
        //Assemble
        Dictionary<string, string> plates = new Dictionary<string, string>();
        plates.Add("CB0281PK", "John Smith");
        plates.Add("OB2981MK", "Suzana Smith");
        plates.Add("PK8174TK", "Rachel Brown");
        plates.Add("PK9999MC", "Rachel Brown");
        plates.Add("PK9174MM", "Rachel Brown");
        plates.Add("PK1001PB", "Rachel Brown");
        plates.Add("OB0198PO", "Racer Racerson");

        string expected = "Rachel Brown: PK9999MC" +
                          $"{Environment.NewLine}\tPK8174TK" +
                          $"{Environment.NewLine}\tPK9174MM" +
                          $"{Environment.NewLine}\tPK1001PB";

        //Act
        string result = TaskSolutions.GetCoolestOwner(plates);

        //Assert
        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestWhenDictionaryIsEmpty()
    {
        //Assember
        Dictionary<string, string> plates = new Dictionary<string, string>();
        string expected = "List must have values! This one is empty";

        //Assert
        Assert.ThrowsException<InvalidDataException>(() => TaskSolutions.GetCoolestOwner(plates), expected); ;
    }

    [TestMethod]
    public void TestCoolestPlateWhenHasOneElement()
    {
        //Assemble
        Dictionary<string, string> plates = new Dictionary<string, string>();
        plates.Add("CB0281PK", "John Smith");

        string expected = "John Smith: CB0281PK";

        //Act
        string result = TaskSolutions.GetCoolestOwner(plates);

        //Assert
        Assert.AreEqual(expected, result);
    }
}