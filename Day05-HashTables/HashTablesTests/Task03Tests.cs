﻿namespace HashTablesTests;
using Day05_HashTables;

[TestClass]
public class Task03Tests
{
    [TestMethod]
    public void TestUniqueElemeents()
    {
        //Assmeble
        string expressoin = "barbarosa";
        char[] expected = new char[] { 'o', 's' };

        //Act
        char[] result = TaskSolutions.GetNonRepeatingChars(expressoin);

        //Assert
        CollectionAssert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestUniqueElemeentsWhenAllRepeat()
    {
        //Assmeble
        string expressoin = "barbara";
        char[] expected = new char[0];

        //Act
        char[] result = TaskSolutions.GetNonRepeatingChars(expressoin);

        //Assert
        CollectionAssert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestGetFirstUniqueElemeentsWhenNA()
    {
        //Assmeble
        string expression = "barbara";
        int expected = -1;

        //Act
        int result = TaskSolutions.GetFirstNonRepeatingChars(expression);

        //Assert
        Assert.AreEqual(expected, result);
    }

}
