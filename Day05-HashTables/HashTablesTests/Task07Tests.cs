﻿using Day05_HashTables;

namespace HashTablesTests;

[TestClass]
public class Task07Tests
{
    [TestMethod]
    public void TestPersonHashCodeGenerator()
    {
        //Assemble
        Person person = new Person("Evgeni", "Dolnibogrov", 19);
        int expected = 22914;

        //Act
        int result = TaskSolutions.GetPersonHashCode(person);

        //Assert
        Assert.AreEqual(expected, result);
    }
}
