﻿namespace Day05_HashTables;

public class Person
{
    public Person(string firstName, string lastName, int age)
    {
        FirstName = firstName;
        LastName = lastName;
        Age = age;
    }

    public string FirstName { get; set; }   
    public string LastName { get; set; }
    public int Age { get; set; }

    public override bool Equals(object? obj)
    {
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
        int hash = 0;
        for (int i = 0; i < FirstName.Length; i++)
        {
            hash += FirstName[i];
        }
        
        for (int i = 0; i < FirstName.Length; i++)
        {
            hash += LastName[i];
        }

        hash *= Age;

        return hash;
    }
}
