﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Day05_HashTables;

public class TaskSolutions
{
    public static string GetCoolestOwner(Dictionary<string, string> plates)
    {
        if (plates.Count == 0)
        {
            throw new InvalidDataException("List must have values! This one is empty");
        }
        else if (plates.Count == 1)
        {
            return $"{plates.First().Value}: {plates.First().Key}";
        }

        StringBuilder sb = new StringBuilder();
        KeyValuePair<string, string> coolestPlate = new KeyValuePair<string, string>();
        int maxNumber = int.MinValue;
        int occurances = 0;
        Dictionary<string, string> multipleCars = new Dictionary<string, string>();
        foreach (var plate in plates)
        {
            int identifier = 0;
            for (int i = 0; i < plate.Key.Length; i++)
            {
                identifier += plate.Key[i];
            }

            identifier += plate.Value.Length;

            if (identifier > maxNumber)
            {
                maxNumber = identifier;
                coolestPlate = plate;
                sb = new StringBuilder();
                sb.AppendLine($"{plate.Value}: {plate.Key}");
                multipleCars = plates.Where(x => x.Value == plate.Value).ToDictionary(k => k.Key, v => v.Value);
                occurances = multipleCars.Count();
            }

        }
        if (occurances > 1)
        {
            foreach (var plate in multipleCars)
            {
                if (coolestPlate.Key != plate.Key)
                {
                    sb.AppendLine($"\t{plate.Key}");
                }
            }
        }

        return sb.ToString().Trim();
    }

    public static T[] GetRecuringElements<T>(T[] array1, T[] array2)
    {
        if (array1.Length < 1 || array2.Length < 1)
        {
            throw new ArgumentNullException("Both arrays must have values");
        }

        HashSet<T> hashSet = new HashSet<T>(array1);
        HashSet<T> returnArray = new HashSet<T>();

        for (int i = 0; i < array2.Length; i++)
        {
            if (hashSet.Contains(array2[i]))
            {
                returnArray.Add(array2[i]);
            }
        }

        return returnArray.ToArray();
    }

    public static char[] GetNonRepeatingChars(string expression)
    {
        HashSet<char> repeatingChars = new HashSet<char>();
        List<char> uniqueChars = new List<char>();

        for (int i = 0; i < expression.Length; i++)
        {
            if (repeatingChars.Contains(expression[i]))
            {
                uniqueChars.Remove(expression[i]);
            }
            else
            {
                uniqueChars.Add(expression[i]);
                repeatingChars.Add(expression[i]);
            }
        }

        return uniqueChars.ToArray();
    }

    public static int GetFirstNonRepeatingChars(string expression)
    {
        char[] uniqueChars = GetNonRepeatingChars(expression);

        if (uniqueChars.Length == 0)
        {
            return -1;
        }

        for (int i = 0; i < expression.Length; i++)
        {
            if (expression[i] == uniqueChars[0])
            {
                return i;
            }
        }

        return -1;
    }

    public static List<string> CheckSpelling(HashSet<string> dictionary, string document)
    {
        if (dictionary.Count == 0 || document == "")
        {
            throw new ArgumentException("Must provide input for both variables!");
        }

        List<string> misspelledWords = new List<string>();
        string[] words = document.Split(' ', StringSplitOptions.RemoveEmptyEntries);

        foreach (string word in words)
        {
            if (!dictionary.Contains(word))
            {
                misspelledWords.Add(word);
            }
        }

        return misspelledWords;
    }

    public static string GroupByAnagrams(List<string> words)
    {
        Dictionary<string, HashSet<string>> anagrams = new Dictionary<string, HashSet<string>>();
        Dictionary<string, SortedDictionary<char, int>> disolvedWords = new Dictionary<string, SortedDictionary<char, int>>();
        foreach (string word in words)
        {
            for (int i = 0; i < word.Length; i++)
            {
                if (!disolvedWords.ContainsKey(word))
                {
                    disolvedWords[word] = new SortedDictionary<char, int>();
                    disolvedWords[word][word[i]] = 0;
                }
                else if (!disolvedWords[word].ContainsKey(word[i]))
                {
                    disolvedWords[word][word[i]] = 0;
                }
                if (disolvedWords.ContainsKey(word))
                {
                    disolvedWords[word][word[i]]++;
                }
            }
        }

        foreach (var word in disolvedWords)
        {
            foreach (var word2 in disolvedWords)
            {
                if (word.Key == word2.Key)
                {
                    continue;
                }
                else if (word.Value.SequenceEqual(word2.Value) && !anagrams.ContainsKey(word.Key))
                {
                    anagrams[word.Key] = new HashSet<string>();

                }
                if (word.Value.SequenceEqual(word2.Value) && anagrams.ContainsKey(word.Key) && !anagrams[word.Key].Contains(word2.Key))
                {
                    anagrams[word.Key].Add(word2.Key);
                }
            }
        }

        HashSet<string> keys = new HashSet<string>();
        foreach (var anagram in anagrams)
        {
            keys.Add(anagram.Key);
        }
        foreach (var key in keys)
        {
            foreach (var word in keys)
            {
                if (anagrams.ContainsKey(key) && anagrams[key].Contains(word))
                {
                    anagrams.Remove(word);
                }
            }
        }

        var pairs = anagrams.Select(p => p.Key + ": " + string.Join(", ", p.Value));
        return string.Join(Environment.NewLine, pairs);
    }

    public static string EncryptPasword(string username, string password)
    {
        string hash = string.Empty;

        using (SHA256 sha256 = SHA256.Create())
        {
            byte[] hashValue = sha256.ComputeHash(Encoding.UTF8.GetBytes(password + username));
            foreach (byte b in hashValue)
            {
                hash += $"{b:X2}";
            }
        }

        return hash;
    }

    public static int GetPersonHashCode(Person person)
    {
        return person.GetHashCode();
    }
}
