﻿namespace Day05_HashTables;

using System.Runtime.Serialization;

public class PersonalHashTable
{
    private const int initialSize = 8;
    private LinkedList<int>[] hashTable = new LinkedList<int>[initialSize];

    public PersonalHashTable()
    {
        Count = 0;
        InitializeHashTable();
    }

    public int Count { get; private set; }

    private void InitializeHashTable()
    {
        for (int i = 0; i < hashTable.Length; i++)
        {
            this.hashTable[i] = new LinkedList<int>();
        }
    }

    private void Resize()
    {
        int newSize = this.hashTable.Length * 2;
        LinkedList<int>[] newHashTable = new LinkedList<int>[newSize];

        for (int i = 0; i < newHashTable.Length; i++)
        {
            newHashTable[i] = this.hashTable[i];
        }

        this.hashTable = newHashTable;
    }

    public void Insert(int element)
    {
        if (this.Count == this.hashTable.Length)
        {
            Resize();
        }

        int index = element % initialSize;

        if (this.hashTable[index].First == null)
        {
            this.hashTable[index].AddFirst(element);
        }
        else if (this.hashTable[index].First.Value != 0)
        {
            this.hashTable[index].AddLast(element);
        }
    }

    public int FindElement(int element)
    {
        int index = element % initialSize;
        LinkedListNode<int> node = this.hashTable[index].First;
        if (node == null)
        {
            return int.MinValue;
        }

        int correctValue = node.Value;
        while (correctValue != element)
        {
            if (node.Next == null)
            {
                return int.MinValue;
            }
            node = node.Next;
            correctValue = node.Value;
        }

        return correctValue;
    }

    public bool Remove(int element)
    {
        int index = element % initialSize;

        return this.hashTable[index].Remove(element);
    }
}
