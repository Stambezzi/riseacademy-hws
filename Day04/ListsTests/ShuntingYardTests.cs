﻿namespace ListsTests;

using Day04Tasks;

[TestClass]
public class ShuntingYardTests
{

    /*
     * ChatGPT generated
     * Input: 3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3
        Output: 3 4 2 * 1 5 - 2 3 ^ ^ / +

        Example 2:
        Input: ( 5 + 3 ) * 12 / 3 - 4
        Output: 5 3 + 12 * 3 / 4 -

        Example 3:
        Input: 2 * ( 3 + 4 ) + 5
        Output: 2 3 4 + * 5 +

        Example 4:
        Input: ( 4 - 1 ) * 5 ^ 2
        Output: 4 1 - 5 2 ^ * 
     */

    [TestMethod]
    public void TestShuntingYardAlgorithm1()
    {
        string expression = "3+4*2/(1-5)^2^3";
        string expected = "342*15-23^^/+";

        string result = ListsHomeWork.ShuntString(expression);

        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestShuntingYardAlgorithmRPN1()
    {
        string expression = "(5+3)*12/3-4";
        string expected = "53+12*3/4-";

        string result = ListsHomeWork.ShuntString(expression);

        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestShuntingYardAlgorithmRPN2()
    {
        string expression = "2*(3+4)+5";
        string expected = "234+*5+";

        string result = ListsHomeWork.ShuntString(expression);

        Assert.AreEqual(expected, result);
    }

    [TestMethod]
    public void TestShuntingYardAlgorithm4()
    {
        string expression = "(4-1)*5^2";
        string expected = "41-52^*";

        string result = ListsHomeWork.ShuntString(expression);

        Assert.AreEqual(expected, result);
    }
}
