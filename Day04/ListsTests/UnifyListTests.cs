using Day04Tasks;
namespace ListsTests;

[TestClass]
public class UnifyListTests
{
    //Testing UnifyList
    [TestMethod]
    public void TestUnifyList()
    {
        //Assign
        List<int> list = new List<int> { 110, 30, 50, 110, 110, 20, 20, 70 };

        //Act
        List<int> result = ListsHomeWork.UnifyList(list);

        //Assert
        CollectionAssert.AllItemsAreUnique(result);
    }
}