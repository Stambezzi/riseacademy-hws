﻿namespace ListsTests;
using Day04Tasks;

[TestClass]
public class HanoiTowerTests
{
    [TestMethod]
    public void TestHonoiTowerResolution()
    {
        //Assign
        int numbers = 10;
        Stack<int> stack = new Stack<int>();
        for (int i = numbers; i > 0; i--)
        {
            stack.Push(i);
        }
        
        //Act
        Stack<int> result = ListsHomeWork.ResolveHanoiTowers(stack);

        //Assert
        CollectionAssert.AreEqual(result, stack);
    }
}
