using System.Text;

namespace Day04Tasks;

public class ListsHomeWork
{
    public static List<T> UnifyList<T>(List<T> list)
    {
        HashSet<T> newList = new HashSet<T>();//correct but there is a HashSet constructor that takes an IEnumerable (which List is)

        foreach (var item in list)
        {
            newList.Add(item);
        }

        return newList.ToList();
    }

    public static void RemoveMiddleElement<T>(LinkedList<T> list)//you don't check for an empty list
    {
        if (list.Count == 0)
        {
            throw new ArgumentException("List is empty");
        }

        int middle = (list.Count / 2); //the variable name is not correct, you even correct for that by substracting from it
        LinkedListNode<T> node = list.First;
        if (node == null)
        {
            throw new ArgumentException("List doesn't contain such a value");
        }

        for (int i = 1; i <= middle; i++)
        {
            node = node.Next;
        }

        list.Remove(node);
    }

    public static string ExpandExpression(string expression)
    {
        Stack<int> nums = new Stack<int>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < expression.Length; i++)
        {
            if (expression[i] == ')')
            {
                break;
            }

            if (char.IsDigit(expression[i]))
            {
                nums.Push(expression[i++] - 48);
                if (nums.Peek() == 0)
                {
                    --i;
                    continue;//so you just ignore it? but it is still left on the stack
                }

                string subExpression = expression[i].ToString();//if the input ends with a digit that is not 0 this will try to access a non-existing char and crash

                if (expression[i] == '(')
                {
                    subExpression = ExpandExpression(expression.Substring(++i));
                    if (expression[i] - 48 == 0)
                    {
                        int j = i;
                        while (expression[j] != ')')
                        {
                            if (expression[j++] - 48 == 0)
                            {
                                ++i;
                            }
                        }
                    }

                    i += subExpression.Length;
                }
                //the algorithm is overly complicated, with some flaws:
                //if there was a recursion call, the base case will ignore everything after its corresponding closing parenthesis
                //0s shouldn't be ignored
                //and you should recurse on every (
                //the recursion is in practice a stack but you should use the data structure, not the call stack
                int iterations = nums.Pop();
                for (int j = 0; j < iterations; j++)
                {
                    sb.Append(subExpression);
                }
            }
            else if (char.IsLetter(expression[i]))
            {
                sb.Append(expression[i]);
            }
        }

        return sb.ToString().Trim();
    }

    public static Stack<int> ResolveHanoiTowers(Stack<int> stack1)
    {
        Stack<int> stack2 = new Stack<int>();
        Stack<int> stack3 = new Stack<int>();

        return stack3;
    }

    public static string ShuntString(string expression)
    {
        StringBuilder sb = new StringBuilder();
        Stack<char> symbols = new Stack<char>();
        for (int i = 0; i < expression.Length; i++)
        {
            if (char.IsDigit(expression[i]))
            {
                sb.Append(expression[i]);
            }
            else if (expression[i] == '+' || expression[i] == '-')
            {
                while (symbols.TryPeek(out char ch) && (ch == '+' || ch == '-' || ch == '*' || ch == '/'))
                {
                    sb.Append(symbols.Pop());
                }

                symbols.Push(expression[i]);
            }
            else if (expression[i] == '*' || expression[i] == '/')
            {
                while (symbols.TryPeek(out char ch) && (ch == '*' || ch == '/'))
                {
                    sb.Append(symbols.Pop());
                }

                symbols.Push(expression[i]);
            }
            else if (expression[i] == '(')
            {
                sb.Append(WorkInBracket(expression, sb, symbols, ref i));
            }
            else if (expression[i] == '^')
            {
                symbols.Push(expression[i]);
            }
        }

        while (symbols.Count > 0)
        {
            sb.Append(symbols.Pop());
        }

        return sb.ToString().Trim();
    }

    private static StringBuilder WorkInBracket(string expression, StringBuilder sb, Stack<char> symbols, ref int i)
    {
        symbols.Push(expression[i++]);
        StringBuilder subBuilder = new StringBuilder();
        while (expression[i] != ')')
        {
            if (char.IsDigit(expression[i]))
            {
                sb.Append(expression[i++]);
            }
            else if (expression[i] == '+' || expression[i] == '-')
            {
                while (symbols.TryPeek(out char ch) && (ch == '+' || ch == '-' || ch == '*' || ch == '/'))
                {
                    sb.Append(symbols.Pop());
                }

                symbols.Push(expression[i++]);
            }
            else if (expression[i] == '*' || expression[i] == '/')
            {
                while (symbols.TryPeek(out char ch) && (ch == '*' || ch == '/'))
                {
                    subBuilder.Append(symbols.Pop());
                }

                symbols.Push(expression[i++]);
            }
            else if (expression[i] == '(')
            {
                symbols.Push(expression[i++]);
            }
            else if (expression[i] == '^')
            {
                symbols.Push(expression[i++]);
            }
        }

        while (symbols.Peek() != '(')
        {
            sb.Append(symbols.Pop());
        }

        symbols.Pop();

        return subBuilder;
    }
}
